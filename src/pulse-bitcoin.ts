import { ethereum, Address, BigInt, BigDecimal, log } from "@graphprotocol/graph-ts"

import { 
  bigIntToBin, 
  currentHalving, 
  getDayFromTimestamp, 
  currentHalvingThreshold
} from "./helpers"

import {
  PulseBitcoin,
  ATMEnd,
  ATMStart,
  MinerEnd,
  MinerStart
} from "../generated/PulseBitcoin/PulseBitcoin"

import { 
  Miner,
  Minter,
  Owner,
  MinerDayData,
  MinterDayData,
  Halving
} from "../generated/schema"

import {
  MinerStore,
  MinterStore
} from "./stores"

export function handleMinerStart(event: MinerStart): void {
  const minerStore = new MinerStore(
    event.params.account,
    bigIntToBin(event.params.data0),
    bigIntToBin(event.params.data1),
    event.params.minerId
  )
  
  let miner = new Miner(minerStore.minerId.toString())
  let owner = new Owner(minerStore.sender.toHexString())

  miner.owner = owner.id
  miner.timestamp = minerStore.timestamp
  miner.bitoshisMiner = minerStore.bitoshisMiner
  miner.pSatoshisMined = minerStore.pSatoshisMined
  miner.bitoshisBurned = minerStore.bitoshisBurned
  miner.bitoshisReturned = minerStore.bitoshisReturned
  miner.startDay = minerStore.day
  miner.endDay = minerStore.day + 30
  miner.hasEnded = false
  miner.endedBy = null

  miner.save()
  owner.save()

  addMinerStartToDayData(minerStore)
  addMinerStartToHalving(event, minerStore)
}

export function handleMinerEnd(event: MinerEnd): void {
  const minerStore = new MinerStore(
    event.params.accountant,
    bigIntToBin(event.params.data0),
    bigIntToBin(event.params.data1),
    event.params.minerId
  )

  let miner = Miner.load(minerStore.minerId.toString())

  if(miner) {
    miner.hasEnded = true
    miner.endedBy = minerStore.sender
    miner.save()

    addMinerEndToDayData(minerStore)
  }
}

export function handleATMStart(event: ATMStart): void {
  const minterStore = new MinterStore(
    event.params.atmId,
    event.params.account,
    bigIntToBin(event.params.data0),
    event.params.tokenAddress
  )

  let minter = new Minter(minterStore.atmId.toString())
  let owner = new Owner(minterStore.sender.toHexString())

  minter.owner = owner.id
  minter.token = minterStore.tokenAddress
  minter.timestamp = minterStore.timestamp
  minter.atmPoints = minterStore.atmPoints
  minter.payout = BigInt.fromI32(0)
  minter.isActive = true

  minter.save()
  owner.save()

  addMinterStartToDayData(minterStore)
}

export function handleATMEnd(event: ATMEnd): void {
  const minterStore = new MinterStore(
    event.params.atmId,
    event.params.account,
    bigIntToBin(event.params.data0),
    null
  )

  let minter = Minter.load(minterStore.atmId.toString())

  if(minter) {
    minter.isActive = false
    minter.payout = minterStore.atmPoints
    minter.save()

    addMinterEndToDayData(minterStore)
  }
}

/*
  #######
  ## Halving Accumulator
  #######
*/

function addMinerStartToHalving(event: MinerStart, miner: MinerStore): void {
  let thisHalving = currentHalving(event.address),
      halvingData = Halving.load(thisHalving.toString())

  if(!halvingData) {
    halvingData = new Halving(thisHalving.toString())
    halvingData.halvingNumber = thisHalving.toI32()
    halvingData.halvingThreshold = currentHalvingThreshold(event.address)
    halvingData.pSatoshisMined = BigInt.fromI32(0)
  }

  halvingData.lastUpdated = miner.timestamp
  halvingData.pSatoshisMined += miner.pSatoshisMined

  halvingData.save()
}

/*
  #######
  ## Day Data Accumulators
  #######
*/

function getOrInstantiateMinerDayData(timestamp: BigInt): MinerDayData {
  let day = getDayFromTimestamp(timestamp)

  let minerDayData = MinerDayData.load(day.toString())
  if(minerDayData == null) {
    minerDayData = new MinerDayData(day.toString())
    
    minerDayData.day = day.toI32()
    
    minerDayData.totalBitoshisStarted = BigInt.fromI32(0)
    minerDayData.totalBitoshisBurned = BigInt.fromI32(0)
    minerDayData.totalBitoshisReturned = BigInt.fromI32(0)
    minerDayData.totalpSatoshisReturned = BigInt.fromI32(0)
  }

  return minerDayData
}

function getOrInstantiateMinterDayData(timestamp: BigInt): MinterDayData {
  let day = getDayFromTimestamp(timestamp)

  let minterDayData = MinterDayData.load(day.toString())
  if(minterDayData == null) {
    minterDayData = new MinterDayData(day.toString())
    
    minterDayData.day = day.toI32()
    
    minterDayData.totalAtmPointsCreated = BigInt.fromI32(0)
    minterDayData.totalASICMinted = BigInt.fromI32(0)
  }

  return minterDayData
}

function addMinerStartToDayData(miner: MinerStore): void {
  let minerDayData = getOrInstantiateMinerDayData(miner.timestamp)
  
  minerDayData.totalBitoshisStarted += miner.bitoshisMiner
  minerDayData.save()
}

function addMinerEndToDayData(miner: MinerStore): void {
  let minerDayData = getOrInstantiateMinerDayData(miner.timestamp)

  minerDayData.totalBitoshisBurned += miner.bitoshisBurned
  minerDayData.totalBitoshisReturned += miner.bitoshisReturned
  minerDayData.totalpSatoshisReturned += miner.pSatoshisMined

  minerDayData.save() 
}

function addMinterStartToDayData(minter: MinterStore): void {
  let minterDayData = getOrInstantiateMinterDayData(minter.timestamp)
  
  minterDayData.totalAtmPointsCreated += minter.atmPoints
  minterDayData.save()
}

function addMinterEndToDayData(minter: MinterStore): void {
  let minterDayData = getOrInstantiateMinterDayData(minter.timestamp)

  minterDayData.totalASICMinted += minter.atmPoints
  minterDayData.save() 
}






