import { ethereum, BigInt, Address, log } from "@graphprotocol/graph-ts"
// import { createMockedFunction } from "matchstick-as"
import { PulseBitcoin } from "../generated/PulseBitcoin/PulseBitcoin"

export var LAUNCH_TIME = BigInt.fromI32(1666286747)

export function binToBigInt(bin: string): BigInt {
  let decimalString = parseInt(bin, 2).toString()

  return BigInt.fromString(
    decimalString.split('.')[0]
  )
}

export function bigIntToBin(bigInt: BigInt): string {
  let binary = "",
      temp = bigInt,
      zero = BigInt.fromI32(0),
      two = BigInt.fromI32(2)

  while(temp > zero){
    if((temp % two) == zero) {
      binary = "0" + binary
    } else {
      binary = "1" + binary
    }

    temp = temp / two
  }

  return binary
}

export function getDayFromTimestamp(timestamp: BigInt): BigInt {
  return timestamp.minus(LAUNCH_TIME).div(BigInt.fromI32(86400))
}

export function currentHalving(address: Address): BigInt {
  // Testing mock function
  // createMockedFunction(
  //   Address.fromString('0xa16081f360e3847006db660bae1c6d1b2e17ec2a'),
  //   'numOfHalvings', 'numOfHalvings():(uint256)'
  // ).returns([ethereum.Value.fromSignedBigInt(BigInt.fromI32(0))]);

  return PulseBitcoin.bind(address).numOfHalvings()
}

export function currentHalvingThreshold(address: Address): BigInt {
  // Testing mock function
  // createMockedFunction(
  //   Address.fromString('0xa16081f360e3847006db660bae1c6d1b2e17ec2a'),
  //   'currentHalvingThreshold', 'currentHalvingThreshold():(uint256)'
  // ).returns([ethereum.Value.fromUnsignedBigInt(BigInt.fromString('10500000000000000000'))]);

  return PulseBitcoin.bind(address).currentHalvingThreshold()
}