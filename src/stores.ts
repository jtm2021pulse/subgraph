import { Address, BigInt, Bytes } from "@graphprotocol/graph-ts"

import { binToBigInt } from "./helpers"

import {
  ATMEnd,
  ATMStart,
  MinerEnd,
  MinerStart
} from "../generated/PulseBitcoin/PulseBitcoin"

export class MinerStore {
  sender: Address
  timestamp: BigInt
  bitoshisMiner: BigInt // uint128
  bitoshisReturned: BigInt // uint128
  pSatoshisMined: BigInt // uint96
  bitoshisBurned: BigInt // uint96
  minerId: BigInt // uint40
  day: i32 // uint24

  /*
    ##########
    ## Event Args
    ##########

    uint256(uint40(block.timestamp)) |
        (uint256(uint24(day)) << 40) | 
        (uint256(uint96(pSatoshisMined)) << 64) |
        (uint256(uint96(bitoshisBurned)) << 160),
    uint256(uint128(bitoshisMiner)) | (uint256(uint128(bitoshisReturned)) << 128),
    msg.sender,
    uint40(newMinerId)
  
    ##########
    ## Unpackin
    ##########

    uint40   timestamp        --> data0 [39  : 0]
    uint24   day              --> data0 [63  : 40]
    uint96   pSatoshisMined   --> data0 [159 : 64]
    uint96   bitoshisBurned   --> data0 [255 : 160]

    uint128  bitoshisMiner    --> data1 [127 : 0]
    uint128  bitoshisReturned --> data1 [255 : 128]
  */
  constructor(sender: Address, data0: string, data1: string, minerId: BigInt) {
    this.sender = sender
    this.minerId = minerId
    this.timestamp = binToBigInt(data0.slice(-39))
    this.day = binToBigInt(data0.slice(-63, -40)).toI32()
    this.pSatoshisMined = binToBigInt(data0.slice(-159, -64))
    this.bitoshisBurned = binToBigInt(data0.slice(-255, -160))
    this.bitoshisMiner = binToBigInt(data1.slice(-127))
    this.bitoshisReturned = binToBigInt(data1.slice(-255, -128))
  }
}

export class MinterStore {
  sender: Address
  timestamp: BigInt
  atmId: BigInt
  atmPoints: BigInt
  tokenAddress: Address

  /*
    ##########
    ## Event Args
    ##########

    uint256(uint40(block.timestamp)) | (uint256(uint216(atmPoints)) << 40),
    msg.sender,
    uint40(atmId),
    tokenAddress

    ##########
    ## Unpackin
    ##########

    uint40   timestamp        --> data0 [39  : 0]
    uint216  atmPoints/payout --> data0 [255  : 40]
  */
  constructor(atmId: BigInt, sender: Address, data0: string, tokenAddress: Address | null) {
    this.sender = sender
    this.atmId = atmId
    this.timestamp = binToBigInt(data0.slice(-39))

    // On end, atmPoints is actually the payout
    this.atmPoints = binToBigInt(data0.slice(-255, -40))
    
    if(tokenAddress) {
      this.tokenAddress = tokenAddress
    } else {
      this.tokenAddress = Address.fromString('0x0000000000000000000000000000000000000000')
    }
  }
}






