import { newMockEvent, createMockedFunction } from "matchstick-as"
import { ethereum, BigInt, Address } from "@graphprotocol/graph-ts"
import {
  PulseBitcoin,
  MinerStart,
  MinerEnd,
  ATMStart,
  ATMEnd
} from "../generated/PulseBitcoin/PulseBitcoin"

export function createATMStartEvent(
  data0: BigInt,
  account: Address,
  atmId: BigInt,
  tokenAddress: Address
): ATMStart {
  let atmStartEvent = changetype<ATMStart>(newMockEvent())

  atmStartEvent.parameters = new Array()

  atmStartEvent.parameters.push(
    new ethereum.EventParam("data0", ethereum.Value.fromUnsignedBigInt(data0))
  )
  atmStartEvent.parameters.push(
    new ethereum.EventParam("account", ethereum.Value.fromAddress(account))
  )
  atmStartEvent.parameters.push(
    new ethereum.EventParam("atmId", ethereum.Value.fromUnsignedBigInt(atmId))
  )
  atmStartEvent.parameters.push(
    new ethereum.EventParam(
      "tokenAddress",
      ethereum.Value.fromAddress(tokenAddress)
    )
  )

  return atmStartEvent
}

export function createATMEndEvent(
  data0: BigInt,
  account: Address,
  atmId: BigInt
): ATMEnd {
  let atmEndEvent = changetype<ATMEnd>(newMockEvent())

  atmEndEvent.parameters = new Array()

  atmEndEvent.parameters.push(
    new ethereum.EventParam("data0", ethereum.Value.fromUnsignedBigInt(data0))
  )
  atmEndEvent.parameters.push(
    new ethereum.EventParam("account", ethereum.Value.fromAddress(account))
  )
  atmEndEvent.parameters.push(
    new ethereum.EventParam("atmId", ethereum.Value.fromUnsignedBigInt(atmId))
  )

  return atmEndEvent
}

export function createMinerStartEvent(
  data0: BigInt,
  data1: BigInt,
  account: Address,
  minerId: BigInt
): MinerStart {
  let minerStartEvent = changetype<MinerStart>(newMockEvent())

  minerStartEvent.parameters = new Array()

  minerStartEvent.parameters.push(
    new ethereum.EventParam("data0", ethereum.Value.fromUnsignedBigInt(data0))
  )
  minerStartEvent.parameters.push(
    new ethereum.EventParam("data1", ethereum.Value.fromUnsignedBigInt(data1))
  )
  minerStartEvent.parameters.push(
    new ethereum.EventParam("account", ethereum.Value.fromAddress(account))
  )
  minerStartEvent.parameters.push(
    new ethereum.EventParam(
      "minerId",
      ethereum.Value.fromUnsignedBigInt(minerId)
    )
  )

  return minerStartEvent
}

export function createMinerEndEvent(
  data0: BigInt,
  data1: BigInt,
  accountant: Address,
  minerId: BigInt
): MinerEnd {
  let minerEndEvent = changetype<MinerEnd>(newMockEvent())

  minerEndEvent.parameters = new Array()

  minerEndEvent.parameters.push(
    new ethereum.EventParam("data0", ethereum.Value.fromUnsignedBigInt(data0))
  )
  minerEndEvent.parameters.push(
    new ethereum.EventParam("data1", ethereum.Value.fromUnsignedBigInt(data1))
  )
  minerEndEvent.parameters.push(
    new ethereum.EventParam(
      "accountant",
      ethereum.Value.fromAddress(accountant)
    )
  )
  minerEndEvent.parameters.push(
    new ethereum.EventParam(
      "minerId",
      ethereum.Value.fromUnsignedBigInt(minerId)
    )
  )

  return minerEndEvent
}
