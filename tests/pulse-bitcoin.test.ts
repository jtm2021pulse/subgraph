import { createMockedFunction } from "matchstick-as"
import { ethereum, BigInt, Address } from "@graphprotocol/graph-ts"
import { PulseBitcoin } from "../generated/PulseBitcoin/PulseBitcoin"

import {
  assert,
  describe,
  test,
  clearStore,
  beforeAll,
  afterAll
} from "matchstick-as/assembly/index"

import {
  handleMinerStart, 
  handleMinerEnd, 
  handleATMStart,
  handleATMEnd
} from "../src/pulse-bitcoin"

import { 
  createMinerStartEvent, 
  createMinerEndEvent, 
  createATMStartEvent,
  createATMEndEvent
} from "./pulse-bitcoin-utils"

describe("MinerStore entity", () => {
  beforeAll(() => {
    let newMinerStartEvent = createMinerStartEvent(
      BigInt.fromString('18268770466636286477546060415871066773340238643200001666295459'),
      BigInt.fromString('1697158305018180586523580854565943954641800000000000000'),
      Address.fromString("0x2CDB9AC4B2591Dcc85aad39Ec389137f8728E5d7"),
      BigInt.fromString('359')
    )

    handleMinerStart(newMinerStartEvent)
  })

  afterAll(() => {
    clearStore()
  })

  test("Miner created at stored", () => {
    assert.entityCount("Miner", 1)
    assert.fieldEquals('Miner', '359', 'owner', '0x2cdb9ac4b2591dcc85aad39ec389137f8728e5d7')
    assert.fieldEquals('Miner', '359', 'id', '359')
    assert.fieldEquals('Miner', '359', 'startDay', '0')
    assert.fieldEquals('Miner', '359', 'endDay', '30')
    assert.fieldEquals('Miner', '359', 'pSatoshisMined', '375000000000000')
    assert.fieldEquals('Miner', '359', 'bitoshisBurned', '12500000000000')
    assert.fieldEquals('Miner', '359', 'bitoshisMiner', '5000000000000000')
    assert.fieldEquals('Miner', '359', 'bitoshisReturned', '4987500000000000')
    assert.fieldEquals('Miner', '359', 'hasEnded', 'false')

    assert.entityCount("MinerDayData", 1)
    assert.fieldEquals("MinerDayData", '0', 'totalBitoshisStarted', '5000000000000000')
  })

  test("Miner ended", () => {

    let newMinerEndEvent = createMinerEndEvent(
      BigInt.fromString('18268770466636286477546060415871066773340238643200001666295459'),
      BigInt.fromString('1697158305018180586523580854565943954641800000000000000'),
      Address.fromString("0x2CDB9AC4B2591Dcc85aad39Ec389137f8728E5d7"),
      BigInt.fromString('359')
    )

    handleMinerEnd(newMinerEndEvent)

    assert.entityCount("Miner", 1)
    assert.fieldEquals('Miner', '359', 'hasEnded', 'true')

    assert.entityCount("MinerDayData", 1)
    assert.fieldEquals("MinerDayData", '0', 'totalBitoshisBurned', '12500000000000')
    assert.fieldEquals("MinerDayData", '0', 'totalBitoshisReturned', '4987500000000000')
    assert.fieldEquals("MinerDayData", '0', 'totalpSatoshisReturned', '375000000000000')

  })
})

describe("MinterStore entity", () => {
  beforeAll(() => {
    // 0xe7f78f8563fa67675c60941e1359d1b3b3b5f3fa71ce1e34172ff351502430ec
    let newMinterStartEvent = createATMStartEvent(
      BigInt.fromString('13197199474172102274622699'),
      Address.fromString("0x21Cc2d5A10830bAbD303418efb4b8FCB80692AA4"),
      BigInt.fromString('1136'),
      Address.fromString("0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48")
    )

    handleATMStart(newMinterStartEvent)
  })

  afterAll(() => {
    clearStore()
  })

  test("Minter created at stored", () => {
    assert.entityCount("Minter", 1)
    assert.fieldEquals('Minter', '1136', 'owner', '0x21cc2d5a10830babd303418efb4b8fcb80692aa4')
    assert.fieldEquals('Minter', '1136', 'token', '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48')
    assert.fieldEquals('Minter', '1136', 'atmPoints', '12002783000000')
    assert.fieldEquals('Minter', '1136', 'isActive', 'true')

    assert.entityCount("MinterDayData", 1)
    assert.fieldEquals("MinterDayData", '3', 'totalAtmPointsCreated', '12002783000000')
  })


  test("Minter ended", () => {

    let newMinterEndEvent = createATMEndEvent(
      BigInt.fromString('13197199474172102274622699'),
      Address.fromString("0x21Cc2d5A10830bAbD303418efb4b8FCB80692AA4"),
      BigInt.fromString('1136')
    )

    handleATMEnd(newMinterEndEvent)

    assert.entityCount("Minter", 1)
    assert.fieldEquals('Minter', '1136', 'isActive', 'false')

    assert.entityCount("MinterDayData", 1)
    assert.fieldEquals("MinterDayData", '3', 'totalASICMinted', '12002783000000')
  })
})

describe("Halving entity", () => {
 beforeAll(() => {
    let newMinerStartEvent = createMinerStartEvent(
      BigInt.fromString('18268770466636286477546060415871066773340238643200001666295459'),
      BigInt.fromString('1697158305018180586523580854565943954641800000000000000'),
      Address.fromString("0x2CDB9AC4B2591Dcc85aad39Ec389137f8728E5d7"),
      BigInt.fromString('359')
    )

    handleMinerStart(newMinerStartEvent)
  })

  afterAll(() => {
    clearStore()
  })

  test("Halving accumulation", () => {
    assert.fieldEquals("Halving", '0', 'halvingNumber', '0')
    assert.fieldEquals("Halving", '0', 'halvingThreshold', '10500000000000000000')
    assert.fieldEquals("Halving", '0', 'pSatoshisMined', '375000000000000')
  })
})

